package com.ipfli.ipf.proximity.analysis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpfProximityAnalysisOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpfProximityAnalysisOneApplication.class, args);
	}

}

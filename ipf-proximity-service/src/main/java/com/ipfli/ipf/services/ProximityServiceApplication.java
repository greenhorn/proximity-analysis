package com.ipfli.ipf.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;


@EnableCaching
@SpringBootApplication
public class ProximityServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProximityServiceApplication.class, args);
    }
}

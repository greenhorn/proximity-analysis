package com.ipfli.ipf.services.processor.distance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DistanceServiceFactory {
    @Autowired
    private DistanceService geodesicDistanceServiceImpl;
    @Autowired
    private DistanceService planarDistanceServiceImpl;
    @Autowired
    private DistanceService plainGeodesicDistanceServiceImpl;

}

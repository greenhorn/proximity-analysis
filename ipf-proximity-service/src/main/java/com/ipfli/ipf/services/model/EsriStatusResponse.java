package com.ipfli.ipf.services.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;


@Getter
@EqualsAndHashCode
public class EsriStatusResponse {
    private final String jobId;
    private final String jobStatus;
    private final String messages;

    public EsriStatusResponse(String jobId, String jobStatus, String message) {
        this.jobId = jobId;
        this.jobStatus = jobStatus;
        this.messages = message;
    }
}

package com.ipfli.ipf.services.model;

public class InputRecord {
    long timestamp;
    String identity;
    double longtitude;
    double lattitude;


    public InputRecord(long timestamp, String identity, double lattitude, double longtitude) {
        super();
        this.timestamp = timestamp;
        this.identity = identity;
        this.longtitude = longtitude;
        this.lattitude = lattitude;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public double getLattitude() {
        return lattitude;
    }

    public void setLattitude(double lattitude) {
        this.lattitude = lattitude;
    }


}

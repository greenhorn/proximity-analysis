package com.ipfli.ipf.services.controller;

import com.ipfli.ipf.services.model.EsriStatusResponse;
import com.ipfli.ipf.services.model.JobStatusCollection;
import com.ipfli.ipf.services.config.AppConfig;
import com.ipfli.ipf.services.config.JobStatus;
import com.ipfli.ipf.services.processor.ColocationJob;
import lombok.extern.slf4j.Slf4j;
import net.sf.geographiclib.Geodesic;
import net.sf.geographiclib.GeodesicLine;
import net.sf.geographiclib.Gnomonic;
import net.sf.geographiclib.GnomonicData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;


@Slf4j
@RestController
public class JobController {
    private Geodesic geod = Geodesic.WGS84;
    private AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
    private TaskExecutor taskExecutor = context.getBean("taskExecutor", TaskExecutor.class);

    @Autowired
    private JobStatusCollection jobStatusCollection;

/*	 
		http://localhost:8080?in_file=%7Burl%3A%22/mnt/IPF/ProximityAnalysis/Input/3abb5760-6349-11ea-8ff8-40b034e3cf9d.csv
		&delimiter=%3B
		&idkey=IDENTITY
		&timestampkey=TIME
		&xkey=LONGITUDE
		&ykey=LATITUDE
		&inclmillisec=true
		&windowSize=30
		&maxDistance=300
		&minimumObjectCount=1
		&maxgap=10
		&filteredids=%5B%5D
		&f=json
		&outgeojsonpath=/mnt/IPF/ProximityAnalysis/Output
*/

    @PostMapping("/submitJob")
    EsriStatusResponse submitJob(@RequestParam("in_file") String inputFilePath,
                                 @RequestParam("delimiter") String delimiter,
                                 @RequestParam("idkey") String idkey,
                                 @RequestParam("timestampkey") String timestampkey,
                                 @RequestParam("xkey") String xkey,
                                 @RequestParam("ykey") String ykey,
                                 @RequestParam("inclmillisec") boolean inclmillisec,
                                 @RequestParam("windowSize") int windowSize,
                                 @RequestParam("maxDistance") int maxDistance,
                                 @RequestParam("minimumObjectCount") int minimumObjectCount,
                                 @RequestParam("maxgap") int maxgap,
                                 @RequestParam("filteredids") String[] filteredids,
                                 @RequestParam("f") String fileType,
                                 @RequestParam("outgeojsonpath") String ouputPath) {
        EsriStatusResponse currResp = null;

        File f = extractFilePath(inputFilePath);
        if (f.exists() && !f.isDirectory()) {
            String jobID = f.getName()
                            .substring(0,
                                       f.getName()
                                        .lastIndexOf('.'));

            ColocationJob currColocationJob = new ColocationJob();
            currColocationJob.setJobStatusCollection(jobStatusCollection);
            currColocationJob.setJobID(jobID);
            currColocationJob.setInputFilePath(inputFilePath);
            currColocationJob.setDelimiter(delimiter.charAt(0));
            currColocationJob.setIdkey(idkey);
            currColocationJob.setTimestampkey(timestampkey);
            currColocationJob.setXkey(xkey);
            currColocationJob.setYkey(ykey);
            currColocationJob.setInclmillisec(inclmillisec);
            currColocationJob.setWindowSize(windowSize);
            currColocationJob.setMaxDistance(maxDistance);
            currColocationJob.setMinimumObjectCount(minimumObjectCount);
            currColocationJob.setMaxgap(maxgap);
            currColocationJob.setFilteredids(filteredids);
            currColocationJob.setFileType(fileType);
            currColocationJob.setOuputPath(ouputPath);

            taskExecutor.execute(currColocationJob);

            currResp = new EsriStatusResponse(jobID, JobStatus.IN_PROGRESS, "");
        } else {
            currResp = new EsriStatusResponse("ERROR",
                                              JobStatus.FAIL,
                                              "File [" + inputFilePath + "] not found or invalid");
            log.warn("Job submission failed. File [" + inputFilePath + "] not found or invalid");
        }

        return currResp;
    }

    private File extractFilePath(String inputFilePath) {
        log.info("Given path: {}", inputFilePath);
        return new File(inputFilePath);
    }

    @GetMapping("/jobs/{id}")
    EsriStatusResponse getJobStatus(@PathVariable String id) {
        EsriStatusResponse currResp = null;

        currResp = jobStatusCollection.getStatus(id);

        if (currResp == null) {
            currResp = new EsriStatusResponse(id, JobStatus.FAIL, "Job [" + id + "] not found!");
            log.warn("Job [" + id + "] not found!");
        }

        return currResp;
    }

    @GetMapping("plannar/{latA}/{longA}/{latB}/{longB}")
    public ResponseEntity getPlannarDistance(@PathVariable double latA,
                                             @PathVariable double longA,
                                             @PathVariable double latB,
                                             @PathVariable double longB) {

        log.info("lat 1: {}, long 1: {}, lat 2: {}, long 2: {}", latA, longA, latB, longB);

        /**
         * {
         *     "lat1": 43.503568,
         *     "lon1": 16.476429,
         *     "azi1": 43.506832,
         *     "lat2": 43.50367556023871,
         *     "lon2": 16.476569259697968,
         *     "azi2": 43.506928554835916,
         *     "s12": 16.476429,
         *     "a12": 1.4827244648829064E-4,
         *     "m12": "NaN",
         *     "M12": "NaN",
         *     "M21": "NaN",
         *     "S12": "NaN"
         * }
         */
//		return ResponseEntity.ok(geod.Direct(latA, longA, latB, longB));

        GeodesicLine geodesicLine = geod.InverseLine(latA, longA, latB, longB);

        /**
         * 362.6388386981342
         */
        geodesicLine = geod.InverseLine(latA, longA, latB, longB);
        Double distance = geodesicLine.Distance();

        log.info("distance: {}", distance);

        Gnomonic gnom = new Gnomonic(geod);
        GnomonicData proj = gnom.Forward(latA, longA, latB, longB);
        log.info("x: {}, y: {}", proj.x, proj.y); //  x: 0.0, y: 362.6388390886769

        return ResponseEntity.ok(distance);
    }

    @GetMapping("geodesic/{latA}/{longA}/{latB}/{longB}")
    public ResponseEntity getGeodesicDistance(@PathVariable double latA,
                                              @PathVariable double longA,
                                              @PathVariable double latB,
                                              @PathVariable double longB) {
        return ResponseEntity.ok(geod.Inverse(latA, longA, latB, longB));
    }
}

package com.ipfli.ipf.services.model;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;


@Slf4j
@Component
public class JobStatusCollection {

    private ConcurrentHashMap<String, EsriStatusResponse> jobStatusCollection = new ConcurrentHashMap<String, EsriStatusResponse>();

    public JobStatusCollection() {
        log.info("JobStatusCollection instance created");
    }

    public void update(String jobId, String jobStatus, String message) {
        jobStatusCollection.put(jobId, new EsriStatusResponse(jobId, jobStatus, message));
    }

    public EsriStatusResponse getStatus(String jobId) {
        return jobStatusCollection.get(jobId);
    }
}

package com.ipfli.ipf.services.config;

public class JobStatus {
    public static final String IN_PROGRESS = "IN-PROGRESS";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAIL = "FAIL";
}

package com.ipfli.ipf.proximity.dao.api;

import com.ipfli.ipf.proximity.model.ProximityAnalysisInvestigation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.ipfli.ipf.proximity.model", "com.ipfli.ipf.proximity.dao.api", "com.ipfli.ipf.proximity.dao.api.impl"})
@Slf4j
public class IpfProximityAnalysisDaoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpfProximityAnalysisDaoApiApplication.class, args);
	}

}

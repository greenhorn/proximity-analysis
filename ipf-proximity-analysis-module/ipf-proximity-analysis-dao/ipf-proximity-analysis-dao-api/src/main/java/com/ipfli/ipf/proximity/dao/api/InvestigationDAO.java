package com.ipfli.ipf.proximity.dao.api;

import com.eaio.uuid.UUID;
import com.ipfli.ipf.proximity.dao.api.model.ProximityAnalysisDataAccessException;
import com.ipfli.ipf.proximity.model.ProximityAnalysisInvestigation;
import com.ipfli.ipm.astyanax.dao.api.GenericColumnDAO;

import java.util.Collection;


public interface InvestigationDAO extends GenericColumnDAO<UUID, Long, ProximityAnalysisInvestigation> {

    ProximityAnalysisInvestigation getInvestigation(UUID id);

    void writeInvestigation(ProximityAnalysisInvestigation metadata) throws ProximityAnalysisDataAccessException;

    void deleteInvestigation(UUID id) throws ProximityAnalysisDataAccessException;

    ProximityAnalysisInvestigation updateInvestigationName(ProximityAnalysisInvestigation updatedInvestigation)
            throws ProximityAnalysisDataAccessException;

    Collection<ProximityAnalysisInvestigation> getAllInvestigations();
}

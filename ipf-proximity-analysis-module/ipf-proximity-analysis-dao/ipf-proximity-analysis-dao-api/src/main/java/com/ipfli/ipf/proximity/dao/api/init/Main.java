package com.ipfli.ipf.proximity.dao.api.init;

import com.ipfli.ipf.proximity.dao.api.InvestigationDAO;
import com.ipfli.ipf.proximity.model.ProximityAnalysisInvestigation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class Main implements CommandLineRunner {

    @Autowired
    private InvestigationDAO investigationDAO;

    @Override
    public void run(String... args) throws Exception {
        List<ProximityAnalysisInvestigation> investigations =
                (List<ProximityAnalysisInvestigation>) investigationDAO.getAllInvestigations();

        log.info("investigations size: {}", investigations.size());
    }
}

package com.ipfli.ipf.proximity.dao.api.configuration;

import com.ipfli.ipf.source.model.Source;
import com.ipfli.ipf.source.model.system.SystemSource;
import com.ipfli.ipm.astyanax.api.CassandraConnector;
import com.ipfli.ipm.astyanax.core.internal.DefaultCassandraConnector;
import com.ipfli.ipm.astyanax.extras.gson.GsonFactory;
import com.ipfli.ipm.astyanax.extras.gson.internal.DynamicGsonFactory;
import com.netflix.astyanax.Keyspace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableConfigurationProperties({CassandraConfig.class})
public class ProximityAnalysisConfig {
    private CassandraConfig cassandraConfig;

    public ProximityAnalysisConfig(CassandraConfig cassandraConfig) {
        this.cassandraConfig = cassandraConfig;
    }

    @Bean
    public CassandraConnector connector() throws Exception {
        DefaultCassandraConnector connector = new DefaultCassandraConnector();
        connector.setSeeds(cassandraConfig.getSeedList());
        connector.setPort(cassandraConfig.getPortNumber());
        connector.setClusterName(cassandraConfig.getClusterName());
        connector.setKeyspaceName(cassandraConfig.getKeySpaceName());
        connector.setStrategy(cassandraConfig.getStrategy());
        connector.setReplicationFactor(cassandraConfig.getReplicationfactor());
        connector.setInitConnectionsPerHost(cassandraConfig.getInitConnsPerHost());
        connector.setMaxConnectionsPerHost(cassandraConfig.getMaxConnsPerHost());
        connector.setConnectTimeout(cassandraConfig.getConnectTimeout());

        connector.initialize();
        return connector;
    }

    @Bean
    public Keyspace getKeyspace(CassandraConnector connector) {
        return connector.getKeyspace();
    }

    /**
     * Look at below for sample:
     * - emerald/itops/emerald-producer-routes/src/main/java/com/ipfli/ipf/emerald/itops/producer/route/internal/TargetProducerRouteBuilder.java
     * - emerald/itops/emerald-producer-routes/src/main/java/com/ipfli/ipf/emerald/itops/producer/route/internal/TargetIdentityProducerRouteBuilder.java
     *
     * @return
     */
    @Bean
    public GsonFactory getGsonFactory() {
        DynamicGsonFactory gsonFactory = new DynamicGsonFactory();
        return gsonFactory;
    }

    @Bean
    public Source getSource() {
        return SystemSource.SYSTEM_SOURCE;
    }
}

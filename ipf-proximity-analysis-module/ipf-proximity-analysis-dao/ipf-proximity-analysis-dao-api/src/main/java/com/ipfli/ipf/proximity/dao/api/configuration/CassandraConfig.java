package com.ipfli.ipf.proximity.dao.api.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "ipm.cassandra")
public class CassandraConfig {

    private String seedList;

    private int portNumber;

    private String clusterName;

    private String keySpaceName;

    private String strategy;

    private int replicationfactor;

    private int initConnsPerHost;

    private int maxConnsPerHost;

    private  int connectTimeout;

    public String getSeedList() {
        return seedList;
    }

    public void setSeedList(String seedList) {
        this.seedList = seedList;
    }

    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getKeySpaceName() {
        return keySpaceName;
    }

    public void setKeySpaceName(String keySpaceName) {
        this.keySpaceName = keySpaceName;
    }

    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    public int getReplicationfactor() {
        return replicationfactor;
    }

    public void setReplicationfactor(int replicationfactor) {
        this.replicationfactor = replicationfactor;
    }

    public int getInitConnsPerHost() {
        return initConnsPerHost;
    }

    public void setInitConnsPerHost(int initConnsPerHost) {
        this.initConnsPerHost = initConnsPerHost;
    }

    public int getMaxConnsPerHost() {
        return maxConnsPerHost;
    }

    public void setMaxConnsPerHost(int maxConnsPerHost) {
        this.maxConnsPerHost = maxConnsPerHost;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }
}

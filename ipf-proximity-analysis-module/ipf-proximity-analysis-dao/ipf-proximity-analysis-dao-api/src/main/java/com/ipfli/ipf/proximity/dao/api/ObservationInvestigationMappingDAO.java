package com.ipfli.ipf.proximity.dao.api;

import com.eaio.uuid.UUID;
import com.ipfli.ipf.proximity.dao.api.model.ProximityAnalysisDataAccessException;
import com.ipfli.ipm.astyanax.dao.api.GenericColumnDAO;

import java.util.Set;


public interface ObservationInvestigationMappingDAO extends GenericColumnDAO<UUID, UUID, String> {

    Set<UUID> getInvestigation(UUID observationId);

    void deleteMapping(UUID investigationId, UUID observationId) throws ProximityAnalysisDataAccessException;

    void createMapping(UUID investigationId, UUID observationId) throws ProximityAnalysisDataAccessException;
}

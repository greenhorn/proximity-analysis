package com.ipfli.ipf.proximity.dao.api.model;

import com.ipfli.ipm.astyanax.dao.api.DAOException;


public class ProximityAnalysisDataAccessException extends DAOException {

    private final String message;

    public ProximityAnalysisDataAccessException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}

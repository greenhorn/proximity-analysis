package com.ipfli.ipf.proximity.dao.api;

import com.eaio.uuid.UUID;
import com.ipfli.ipf.proximity.dao.api.model.ProximityAnalysisDataAccessException;
import com.ipfli.ipf.proximity.model.ProximityAnalysisInvestigationResult;
import com.ipfli.ipm.astyanax.dao.api.GenericColumnDAO;

import java.util.List;


public interface InvestigationResultDAO extends GenericColumnDAO<UUID, Long, ProximityAnalysisInvestigationResult> {
    List<ProximityAnalysisInvestigationResult> getResult(UUID observationId);

    void deleteResult(UUID observationID) throws ProximityAnalysisDataAccessException;

    void writeResult(UUID investigationId, List<ProximityAnalysisInvestigationResult> results) throws ProximityAnalysisDataAccessException;
}

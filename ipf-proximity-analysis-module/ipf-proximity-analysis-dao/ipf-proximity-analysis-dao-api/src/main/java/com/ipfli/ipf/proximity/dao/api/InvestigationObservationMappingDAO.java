package com.ipfli.ipf.proximity.dao.api;

import com.eaio.uuid.UUID;
import com.ipfli.ipf.proximity.dao.api.model.ProximityAnalysisDataAccessException;
import com.ipfli.ipm.astyanax.dao.api.GenericColumnDAO;

import java.util.Set;


public interface InvestigationObservationMappingDAO extends GenericColumnDAO<UUID, UUID, String> {

    Set<UUID> getAllObservations(UUID investigationId);

    void deleteMapping(UUID investigationId, UUID observationId) throws ProximityAnalysisDataAccessException;

    void createMapping(UUID investigationId, UUID observationId) throws ProximityAnalysisDataAccessException;
}

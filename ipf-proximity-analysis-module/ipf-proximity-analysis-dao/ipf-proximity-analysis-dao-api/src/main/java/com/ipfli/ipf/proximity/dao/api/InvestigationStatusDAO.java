package com.ipfli.ipf.proximity.dao.api;

import com.eaio.uuid.UUID;
import com.ipfli.ipf.proximity.dao.api.model.ProximityAnalysisDataAccessException;
import com.ipfli.ipf.proximity.model.ProximityAnalysisInvestigationStatus;
import com.ipfli.ipm.astyanax.dao.api.GenericColumnDAO;


public interface InvestigationStatusDAO extends GenericColumnDAO<UUID, Long, String> {
    ProximityAnalysisInvestigationStatus getStatus(UUID investigationId);

    void writeStatus(UUID id, ProximityAnalysisInvestigationStatus status) throws ProximityAnalysisDataAccessException;

    void delete(UUID id) throws ProximityAnalysisDataAccessException;
}

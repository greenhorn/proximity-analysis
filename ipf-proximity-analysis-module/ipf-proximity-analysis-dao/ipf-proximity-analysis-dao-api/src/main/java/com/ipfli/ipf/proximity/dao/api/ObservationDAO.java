package com.ipfli.ipf.proximity.dao.api;

import com.eaio.uuid.UUID;
import com.ipfli.ipf.proximity.dao.api.model.ProximityAnalysisDataAccessException;
import com.ipfli.ipf.proximity.model.ProximityAnalysisObservation;
import com.ipfli.ipm.astyanax.dao.api.GenericColumnDAO;


public interface ObservationDAO extends GenericColumnDAO<UUID, Long, ProximityAnalysisObservation> {

    ProximityAnalysisObservation getObservation(UUID observationId);

    void deleteObservation(UUID observationID) throws ProximityAnalysisDataAccessException;

    void writeObservation(ProximityAnalysisObservation observation) throws ProximityAnalysisDataAccessException;
}

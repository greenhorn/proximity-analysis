package com.ipfli.ipf.proximity.dao.api.impl;

import com.eaio.uuid.UUID;
import com.ipfli.ipf.proximity.dao.api.InvestigationDAO;
import com.ipfli.ipf.proximity.dao.api.model.ProximityAnalysisDataAccessException;
import com.ipfli.ipf.proximity.model.ProximityAnalysisInvestigation;
import com.ipfli.ipm.astyanax.api.model.ColumnFamilyDefinition;
import com.ipfli.ipm.astyanax.api.model.DataType;
import com.ipfli.ipm.astyanax.dao.api.DAOException;
import com.ipfli.ipm.astyanax.dao.api.RecordNotFoundException;
import com.ipfli.ipm.astyanax.dao.core.AbstractColumnDAO;
import com.ipfli.ipm.astyanax.extras.eaio.UUIDJavaType;
import com.ipfli.ipm.astyanax.extras.gson.GsonDataType;
import com.ipfli.ipm.astyanax.extras.gson.GsonFactory;
import com.netflix.astyanax.Keyspace;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class InvestigationDAOImpl extends AbstractColumnDAO<UUID, Long, ProximityAnalysisInvestigation> implements InvestigationDAO {

    private static final String CF_OBSERVATION_IDENTITIES = "ProximityAnalysis_Investigation";

    @Autowired
    public InvestigationDAOImpl(final Keyspace keyspace, final GsonFactory factory) {
        super(keyspace, createColumnFamily(factory));
    }

    private static ColumnFamilyDefinition<UUID, Long, ProximityAnalysisInvestigation> createColumnFamily(GsonFactory gsonFactory) {
        return new ColumnFamilyDefinition<>(CF_OBSERVATION_IDENTITIES,
                                            new UUIDJavaType(false),
                                            DataType.LONG,
                                            GsonDataType.of(ProximityAnalysisInvestigation.class, gsonFactory));
    }

    @Override
    public ProximityAnalysisInvestigation getInvestigation(UUID id) {
        Map<Long, ProximityAnalysisInvestigation> investigationMap = new HashMap<>();

        try {
            investigationMap.putAll(this.createQuery().find(id));
        }
        catch (RecordNotFoundException ex) {
            log.trace("No investigation found with id {}", id.toString());
            return null;
        }
        if (investigationMap.isEmpty()) {
            log.trace("No investigation found with id:{}", id);
            return null;
        }
        else return investigationMap.entrySet().iterator().next().getValue();
    }

    @Override
    public void writeInvestigation(ProximityAnalysisInvestigation investigation) throws ProximityAnalysisDataAccessException {
        try {
            this.write(investigation.getInvestigationId(), investigation.getCreationDate(), investigation);
        }
        catch (DAOException exception) {
            throw new ProximityAnalysisDataAccessException(String.format("Exception occurred while creating investigation %s",
                                                                         String.valueOf(investigation.getInvestigationId())));
        }
    }

    @Override
    public void deleteInvestigation(UUID id) throws ProximityAnalysisDataAccessException {
        try {
            this.remove(id, getInvestigation(id).getCreationDate());
        }
        catch (DAOException exception) {
            throw new ProximityAnalysisDataAccessException(String.format("Exception occurred while deleting investigation %s",
                                                                         String.valueOf(id)));
        }
    }

    @Override
    public ProximityAnalysisInvestigation updateInvestigationName(ProximityAnalysisInvestigation updatedInvestigation)
            throws ProximityAnalysisDataAccessException {
        deleteInvestigation(updatedInvestigation.getInvestigationId());
        writeInvestigation(updatedInvestigation);
        return null;
    }

    @Override
    public Collection<ProximityAnalysisInvestigation> getAllInvestigations() {
        try {
            return this.createQuery().findAll().values().stream().map(entry -> entry.values()).flatMap(Collection::stream).collect(
                    Collectors.toList());
        }
        catch (RecordNotFoundException ex) {
            log.trace("No investigation found in database");
            return Collections.EMPTY_LIST;
        }
    }
}
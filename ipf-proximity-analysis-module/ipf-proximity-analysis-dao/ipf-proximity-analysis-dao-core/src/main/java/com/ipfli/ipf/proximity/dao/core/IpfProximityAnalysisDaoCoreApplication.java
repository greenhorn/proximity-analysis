package com.ipfli.ipf.proximity.dao.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpfProximityAnalysisDaoCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpfProximityAnalysisDaoCoreApplication.class, args);
	}

}

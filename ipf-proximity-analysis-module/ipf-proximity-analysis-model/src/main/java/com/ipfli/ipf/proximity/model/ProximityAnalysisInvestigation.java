package com.ipfli.ipf.proximity.model;

import com.eaio.uuid.UUID;
import com.ipfli.ipf.source.model.Source;
import com.ipfli.ipf.source.model.SourceAware;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Optional;

@Setter
@Getter
public class ProximityAnalysisInvestigation implements SourceAware<Source>, Serializable {

    private final UUID id;
    private final String investigationName;
    private final Long creationDate;
    private final Long modificationDate;
    private final Source source;

    public ProximityAnalysisInvestigation(UUID id, String investigationName, Long creationDate, Long modificationDate, Source source) {
        this.id = id;
        this.investigationName = investigationName;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
        this.source = source;
    }

    public ProximityAnalysisInvestigation(ProximityAnalysisInvestigation that, String newName) {
        this.id = that.id;
        this.investigationName = newName;
        this.creationDate = that.creationDate;
        this.modificationDate = that.modificationDate;
        this.source = that.source;
    }

    @Override
    public Optional<Source> getSource() {
        return Optional.empty();
    }

    public UUID getInvestigationId() {
        return id;
    }
}

package com.ipfli.ipf.proximity.model;

public enum ProximityAnalysisInvestigationStatus {
    CREATED("created"), IN_PROGRESS("in progress"), COMPLETED("completed"), FAILED("failed");
    private String status;

    ProximityAnalysisInvestigationStatus(String status) {
        this.status = status;
    }

    public static ProximityAnalysisInvestigationStatus getStatus(String value) {
        if (value == null || value.trim().isEmpty()) return null;
        for (ProximityAnalysisInvestigationStatus status : ProximityAnalysisInvestigationStatus.values()) {
            if (status.toString().equalsIgnoreCase(value)) return status;
        }
        return null;
    }

    @Override
    public String toString() {
        return status;
    }
}

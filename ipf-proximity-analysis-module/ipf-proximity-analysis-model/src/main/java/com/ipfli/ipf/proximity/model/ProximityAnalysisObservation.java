package com.ipfli.ipf.proximity.model;

import com.eaio.uuid.UUID;
import com.ipfli.ipf.target_manager.model.Target;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class ProximityAnalysisObservation implements Serializable {

    private UUID observationId;
    private List<String> observedIdentities;
    private Target observedTarget;
    private Long creationDate;

    public ProximityAnalysisObservation(UUID observationId, Long creationDate) {
        this.observationId = observationId;
        this.creationDate = creationDate;
        observedIdentities = new ArrayList<>();
    }
}

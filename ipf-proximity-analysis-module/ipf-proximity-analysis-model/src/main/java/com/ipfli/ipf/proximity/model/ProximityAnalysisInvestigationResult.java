package com.ipfli.ipf.proximity.model;

import lombok.Getter;
import lombok.Setter;
import org.javatuples.Pair;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
public class ProximityAnalysisInvestigationResult implements Serializable {
    private Integer id;
    private Long startTimestamp;
    private String formattedStartTimestamp;
    private Long endTimestamp;
    private String formattedEndTimestamp;
    private List<String> objects;
    private Double distance;
    private Pair<Double, Double> coordinates;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProximityAnalysisInvestigationResult result = (ProximityAnalysisInvestigationResult) o;
        return startTimestamp.equals(result.startTimestamp) && formattedStartTimestamp.equals(result.formattedStartTimestamp) &&
               endTimestamp.equals(result.endTimestamp) && formattedEndTimestamp.equals(result.formattedEndTimestamp) && objects.equals(
                result.objects) && distance.equals(result.distance) && coordinates.equals(result.coordinates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startTimestamp, formattedStartTimestamp, endTimestamp, formattedEndTimestamp, objects, distance, coordinates);
    }

    @Override
    public String toString() {
        return "ProximityAnalysisInvestigationResult{" + "startTimestamp=" + startTimestamp + ", formattedStartTimestamp='" +
               formattedStartTimestamp + '\'' + ", endTimestamp=" + endTimestamp + ", formattedEndTimestamp='" + formattedEndTimestamp +
               '\'' + ", objects=" + objects + ", distance=" + distance + ", coordinates=" + coordinates + '}';
    }
}

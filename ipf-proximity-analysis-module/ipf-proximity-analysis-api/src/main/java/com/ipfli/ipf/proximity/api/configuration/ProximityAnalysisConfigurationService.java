package com.ipfli.ipf.proximity.analysis.api.configuration;

/**
 * Managed service to read proximity analysis related configuration
 */
public interface ProximityAnalysisConfigurationService {
    /**
     * Retrieves IP address of Esri server.
     *
     * @return the IP address of Esri server
     */
    String getEsriServerAddress();

    /**
     * Retrieves timeout for Esri requests.
     *
     * @return the timeout for Esri requests
     */
    Long getTimeoutInMin();

    /**
     * Retrieves foler path for creating CSV file in IPF backend.
     *
     * @return path for creating CSV file in IPF backend
     */
    String getIpfInputFolderPath();

    /**
     * Retrieves folder path for reading json file in IPF backend.
     *
     * @return path for reading json file in IPF backend
     */
    String getIpfOutputFolderPath();

    /**
     * Retrieves folder path for writing CSV file on ESRI server.
     *
     * @return path for writing CSV file on ESRI server
     */
    String getEsriServerInputPath();

    /**
     * Retrieves port for ESRI server.
     *
     * @return port for ESRI server
     */
    String getEsriServerPort();

    /**
     * Retrieves folder path for reading result json file on ESRI server.
     *
     * @return path for reading result json file on ESRI server
     */
    String getEsriServerOutputPath();
}

package com.ipfli.ipf.proximity.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpfProximityAnalysisApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpfProximityAnalysisApiApplication.class, args);
	}

}

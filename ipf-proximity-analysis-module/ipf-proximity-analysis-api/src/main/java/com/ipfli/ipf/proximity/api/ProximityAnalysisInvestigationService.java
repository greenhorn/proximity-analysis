package com.ipfli.ipf.proximity.api;

import com.ipfli.ipf.proximity.model.ProximityAnalysisInvestigation;
import com.ipfli.ipf.proximity.model.ProximityAnalysisInvestigationResult;
import com.ipfli.ipf.proximity.model.ProximityAnalysisInvestigationStatus;
import com.ipfli.ipf.proximity.model.ProximityAnalysisObservation;
import com.ipfli.ipf.source.model.Source;

import java.util.Collection;


/**
 * Service to expose proximity analysis investigation APIs.
 */
public interface ProximityAnalysisInvestigationService {
    /**
     * Retrieves all investigation for the current user.
     *
     * @return all investigation for the current user
     */
    Collection<ProximityAnalysisInvestigation> getAllInvestigations();

    /**
     * Retrieves all investigation for the current user filtered by the name.
     *
     * @param name the name to filter the results by
     * @return all investigation for the current user filtered by the name
     */
    Collection<ProximityAnalysisInvestigation> getFilteredInvestigations(String name);

    /**
     * Retrieves {@link ProximityAnalysisInvestigation} details for the investigation.
     *
     * @param investigationId the investigation identifier
     * @return the details of the investigation, {@code null} if no such investigation exists
     */
    ProximityAnalysisInvestigation getInvestigationDetailsById(String investigationId);

    /**
     * Creates a new investigation
     *
     * @param investigationName the name of the investigation to be created
     * @param creationTime      the creation time in milliseconds
     * @param source            the source
     * @return {@link ProximityAnalysisInvestigation} instance, {@code null} if any exception occurs
     */
    ProximityAnalysisInvestigation createNewInvestigation(String investigationName, Long creationTime, Source source);

    /**
     * Retrieves all {@link com.ipfli.ipf.proximity.model.ProximityAnalysisObservation} for the investigation.
     *
     * @param investigationId the investigation identifier
     * @return collection of observation for the investigation
     */
    Collection<ProximityAnalysisObservation> getAllObservations(String investigationId);

    /**
     * Retrieves {@link ProximityAnalysisObservation} for the observation.
     *
     * @param observationId the observation identifier
     * @return {@link ProximityAnalysisObservation} instance, {@code null} if no such observation exists
     */
    ProximityAnalysisObservation getObservationSpecDetails(String observationId);

    /**
     * Renames an investigation.
     *
     * @param investigationId the investigation identifier
     * @param newName         the new name of the investigation
     * @return {@link ProximityAnalysisInvestigation} instance, {@code null} if any exception occurs
     */
    ProximityAnalysisInvestigation renameInvestigation(String investigationId, String newName);

    /**
     * Deletes an investigation.
     *
     * @param investigationId the investigation identifier
     * @return {@code true} if investigation is deleted successfully, {@code false} otherwise
     */
    boolean deleteInvestigation(String investigationId);

    /**
     * Deletes an observation.
     *
     * @param investigationId the investigation identifier
     * @param observationId   the observation identifier
     * @return {@code true} if observation is deleted successfully, {@code false} otherwise
     */
    boolean deleteObservationSpec(String investigationId, String observationId);

    /**
     * Creates a new target observation.
     *
     * @param investigationId  the investigation identifier
     * @param targetIdentifier the target identifier
     * @return instance of {@link ProximityAnalysisObservation}, {@code null} if any exception occur
     */
    ProximityAnalysisObservation createTargetObservation(String investigationId, String targetIdentifier);

    /**
     * Creates a new target observation.
     *
     * @param investigationId the investigation identifier
     * @param identityCaption the identity caption
     * @param identityNature  the identity nature
     * @param identityService the identity service
     * @return instance of {@link ProximityAnalysisObservation}, {@code null} if any exception occur
     */
    ProximityAnalysisObservation createIdentityObservation(String investigationId,
                                                           String identityCaption,
                                                           String identityNature,
                                                           String identityService);

    /**
     * Retrieves status of an investigation.
     *
     * @param investigationId the investigation identifier
     * @return status of an investigation, {@code null} if any exception occur
     */
    ProximityAnalysisInvestigationStatus getInvestigationStatus(String investigationId);

    /**
     * Triggers a proximity analysis on Esri server.
     *
     * @param investigationId the investigation identifier
     * @param fromDate        the from date
     * @param toDate          the to date
     * @param timeSlice       the time slice
     * @param maxDistance     the maximum distance
     * @param maxGap          the maximum gap
     * @param minimumEntities the minimum count of entity
     * @return {@code true} if proximity analysis has been successfully triggered on Esri server, {@code false} otherwise
     */
    boolean triggerProximityAnalysis(String investigationId,
                                     Long fromDate,
                                     Long toDate,
                                     Long timeSlice,
                                     Long maxDistance,
                                     Long maxGap,
                                     Integer minimumEntities);

    /**
     * Retrieves the results of an investigation.
     *
     * @param investigationId the investigation identifier
     * @return {@link com.ipfli.ipf.proximity.model.ProximityAnalysisInvestigationResult}s, {@code null} otherwise
     */
    Collection<ProximityAnalysisInvestigationResult> getInvestigationResult(String investigationId);
}

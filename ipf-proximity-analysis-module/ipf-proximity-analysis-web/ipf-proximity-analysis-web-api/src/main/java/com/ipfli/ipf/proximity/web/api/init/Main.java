package com.ipfli.ipf.proximity.web.api.init;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class Main implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        log.info("A B C");
    }
}

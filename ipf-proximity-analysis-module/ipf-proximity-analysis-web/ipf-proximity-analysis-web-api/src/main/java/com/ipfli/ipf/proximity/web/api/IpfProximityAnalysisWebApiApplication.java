package com.ipfli.ipf.proximity.web.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpfProximityAnalysisWebApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpfProximityAnalysisWebApiApplication.class, args);
	}

}

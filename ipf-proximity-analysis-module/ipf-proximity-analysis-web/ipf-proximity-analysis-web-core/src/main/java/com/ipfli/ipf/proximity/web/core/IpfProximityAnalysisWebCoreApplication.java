package com.ipfli.ipf.proximity.web.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpfProximityAnalysisWebCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpfProximityAnalysisWebCoreApplication.class, args);
	}

}

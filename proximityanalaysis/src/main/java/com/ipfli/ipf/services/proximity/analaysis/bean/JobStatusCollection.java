package com.ipfli.ipf.services.proximity.analaysis.bean;

import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

@Scope("singleton")
public class JobStatusCollection {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(JobStatusCollection.class);	
	private ConcurrentHashMap<String, EsriStatusResponse> jobStatusCollection = new ConcurrentHashMap<String, EsriStatusResponse>();
	
    public JobStatusCollection() {
    	LOGGER.info("JobStatusCollection instance created");
    }
    
	public ConcurrentHashMap<String, EsriStatusResponse> getJobStatusCollection() {
		return jobStatusCollection;
	}
	public void setJobStatusCollection(ConcurrentHashMap<String, EsriStatusResponse> jobStatusCollection) {
		this.jobStatusCollection = jobStatusCollection;
	}
	
}

package com.ipfli.ipf.services.proximity.analaysis.bean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProximityResult {
	private String type;
	private Crs crs;
	private List<Feature> features;	

	public ProximityResult() {
		this.type = "FeatureCollection";
		
		this.crs = new Crs();
		this.crs.type = "name";
		this.crs.properties.name = "EPSG:4326";
		
		this.features = new ArrayList<Feature>();		
	}
	
	
	public void addProximityResult(int sequenceId, double latitude, double longtitude, long startTimestamp, long endTimestamp, String foundidentities, double disatance) {
		Feature currFeature = new Feature();
		
		currFeature.type = "Feature";
		currFeature.id = sequenceId;
		
		currFeature.geometry.type = "Point";
		
		currFeature.geometry.coordinates.add(latitude);
		currFeature.geometry.coordinates.add(longtitude);
		
		//"formattedEndTimestamp": "2020-05-17 08:00:01",	
		DateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		currFeature.properties.startTimestamp = startTimestamp*1000;
		currFeature.properties.endTimestamp = endTimestamp*1000;
		currFeature.properties.formattedStartTimestamp = f.format(new Date(startTimestamp*1000));
		currFeature.properties.formattedEndTimestamp = f.format(new Date(endTimestamp*1000));
		currFeature.properties.objects = foundidentities;
		currFeature.properties.distance = disatance;		
		
		this.features.add(currFeature);
	}
	

	private class Crs {
		public String type;
		public CRsProperties properties = new CRsProperties();;
	}
	private class CRsProperties {
		public String name;
	}
	
	
	private class Geometry{
	    public String type;
	    public List<Double> coordinates = new ArrayList<Double>();
	}

	private class Feature {
		public String type;
		public int id;
		public Geometry geometry = new Geometry();
		public FeatureProperties properties = new FeatureProperties();;
	}
	private class FeatureProperties {
		public long startTimestamp;
		public String formattedStartTimestamp;
		public long endTimestamp;
		public String formattedEndTimestamp;
		public String objects;
		public double distance;
	}	

}

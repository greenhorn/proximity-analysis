package com.ipfli.ipf.services.proximity.analaysis.bean;

public class EsriStatusResponse {
	private String jobId;
	private String jobStatus;
	private String messages;
	
	public EsriStatusResponse (String jobId, String jobStatus, String message) {
		this.jobId = jobId;
		this.jobStatus = jobStatus;
		this.messages = message;
	}
	
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
	public String getMessages() {
		return messages;
	}
	public void setMessages(String messages) {
		this.messages = messages;
	}
	
	
}

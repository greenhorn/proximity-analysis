package com.ipfli.ipf.services.proximity.analaysis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProximityanalaysisApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProximityanalaysisApplication.class, args);
	}

}

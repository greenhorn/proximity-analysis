package com.ipfli.ipf.services.proximity.analaysis.processor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.ipfli.ipf.services.proximity.analaysis.bean.EsriStatusResponse;
import com.ipfli.ipf.services.proximity.analaysis.bean.InputRecord;
import com.ipfli.ipf.services.proximity.analaysis.bean.JobStatusCollection;
import com.ipfli.ipf.services.proximity.analaysis.bean.ProximityResult;
import com.ipfli.ipf.services.proximity.analaysis.config.AppConfig;
import com.ipfli.ipf.services.proximity.analaysis.config.CommonReference;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import net.sf.geographiclib.Geodesic;
import net.sf.geographiclib.GeodesicLine;
import net.sf.geographiclib.GeodesicMask;

@Component
@Scope("prototype")
public class ColocationJob implements Runnable {

	private static final Logger LOGGER = LoggerFactory.getLogger(ColocationJob.class);
	private final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	private static Geodesic geod = Geodesic.WGS84;

	private String jobID;
	private String inputFilePath;
	private char delimiter;
	private String idkey;
	private String timestampkey;
	private String xkey;
	private String ykey;
	private boolean inclmillisec;
	private int windowSize;
	private int maxDistance;
	private int minimumObjectCount;
	private int maxgap;
	private String[] filteredids;
	private String fileType;
	private String ouputPath;

	private JobStatusCollection jobStatusCollection;
	private DateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
	private SortedMap<Long, InputRecord> recordHolder = new TreeMap<Long, InputRecord>();
	private Connection connection;
	private String tableName = "";
	private String logHeader = "";
	private long startTime = 0;
	private long lastTimestamp = 0;
	ProximityResult proximityResult = new ProximityResult();
	HashMap<Integer, Long> proximityResultHash = new HashMap<Integer, Long>();
	Gson gson = new Gson();
	private int meetingCounter = 0;

	public void run() {
		logHeader = "JOb [" + jobID + "] ";
		tableName = "T" + jobID.replace("-", "");

		jobStatusCollection.getJobStatusCollection().put(jobID,
				new EsriStatusResponse(jobID, CommonReference.ESRI_STATUS_PROCESSING, ""));
		LOGGER.debug(logHeader + " status added");

		startTime = java.lang.System.currentTimeMillis();

		/* Read CSV file line by line and added to hashmap */
		CSVReader reader = null;
		CSVReaderBuilder builder = null;

		try {
			Class.forName("org.h2.Driver");
			connection = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "");
			createTable(tableName);

			try {
				builder = new CSVReaderBuilder(new FileReader(inputFilePath))
						.withCSVParser(new CSVParserBuilder().withSeparator(delimiter).build());
				reader = builder.withSkipLines(1).build();

				String insertQuery = "INSERT INTO " + tableName + "(timestamp, identity, latitude, longtitude)"
						+ " VALUES" + "(?, ?, ?, ?)";
				PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);

				/* 016S8759026200517006006SMSISDN;43.511359;16.480579;2020-05-17 08:00:06.000 */
				/* Load record into in memory database */
				String[] currLineArry;
				for (int counter = 0; (currLineArry = reader.readNext()) != null; counter++) {
					Date currLineDate = formatter.parse(currLineArry[3]);
					long currLineTimestamp = currLineDate.getTime() / 1000;

					if (currLineTimestamp > lastTimestamp) {
						lastTimestamp = currLineTimestamp;
					}

					preparedStatement.setLong(1, currLineTimestamp);
					preparedStatement.setString(2, currLineArry[0]);
					preparedStatement.setDouble(3, Double.parseDouble(currLineArry[1]));
					preparedStatement.setDouble(4, Double.parseDouble(currLineArry[2]));
					preparedStatement.addBatch();
					preparedStatement.clearParameters();

					if (counter == 499) {
						int[] controlledInserted = preparedStatement.executeBatch();
						LOGGER.debug(logHeader + controlledInserted.length + " record inserted");
						counter = 0;
						preparedStatement.clearBatch();
					}
				}

				int[] finalInserted = preparedStatement.executeBatch();
				LOGGER.debug(logHeader + finalInserted.length + " record inserted");
				preparedStatement.close();
				preparedStatement = null;

				LOGGER.info(logHeader + " Records loaded into database");

			} catch (FileNotFoundException e) {
				jobStatusCollection.getJobStatusCollection().put(jobID, new EsriStatusResponse(jobID,
						CommonReference.ESRI_STATUS_FAILED, "File [" + inputFilePath + "] not found or invalid"));
				LOGGER.error(logHeader + " Job submission failed. File [" + inputFilePath + "] not found or invalid");
			} catch (IOException e) {
				jobStatusCollection.getJobStatusCollection().put(jobID, new EsriStatusResponse(jobID,
						CommonReference.ESRI_STATUS_FAILED, "[" + inputFilePath + "] : IO Exception"));
				LOGGER.error(logHeader + "  File [" + inputFilePath + "]: IO Exception", e);
			} catch (CsvValidationException e) {
				jobStatusCollection.getJobStatusCollection().put(jobID, new EsriStatusResponse(jobID,
						CommonReference.ESRI_STATUS_FAILED, "[" + inputFilePath + "] : Invalid CSV format detected"));
				LOGGER.error(logHeader + "  [" + inputFilePath + "] : Invalid CSV format detected", e);
			} catch (ParseException e) {
				jobStatusCollection.getJobStatusCollection().put(jobID, new EsriStatusResponse(jobID,
						CommonReference.ESRI_STATUS_FAILED, "[" + inputFilePath + "] : Invalid date format detected"));
				LOGGER.error(logHeader + " [" + inputFilePath + "] : Invalid CSV format detected", e);
			} finally {
				builder = null;
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						LOGGER.error(logHeader + " [" + inputFilePath + "] : close reader error", e);
					}
					reader = null;
				}
			}

			/* Get timestamp sort in asc */
			List<InputRecord> sortedRecords = getAllRecordsSortByTimestamp(tableName);

			/* Loop tree map key */
			Iterator<InputRecord> sortedRecordsIterator = sortedRecords.iterator();
			while (sortedRecordsIterator.hasNext()) {

				InputRecord sortedRecord = sortedRecordsIterator.next();
				long maxTimeStamp = sortedRecord.getTimestamp() + (this.windowSize * 60);
				TreeSet<String> meetingIdentities = new TreeSet<String>();
				double maxDistanceAmongMeeting = 0;
				long meetingEndTimestamp = 0;

				/*
				 * Check if max timestamp is more than last record (sorted) timestamp, then use
				 * it
				 */
				if (maxTimeStamp > lastTimestamp) {
					maxTimeStamp = lastTimestamp;
				}
				LOGGER.debug(logHeader + " Current timestamp [" + sortedRecord.getTimestamp() + "], maxtimestamp ["
						+ maxTimeStamp + "]");

				/* Add current identity into list */
				meetingIdentities.add(sortedRecord.getIdentity());

				/* Get records between the current record timestamp and max timestamp */
				ArrayList<InputRecord> currRelaventRecords = getRecordByTimestampPeriod(tableName,
						sortedRecord.getIdentity(), sortedRecord.getTimestamp(), maxTimeStamp);
				Iterator<InputRecord> currRelaventRecordsIterator = currRelaventRecords.iterator();

				while (currRelaventRecordsIterator.hasNext()) {

					InputRecord currRelevanceRecord = currRelaventRecordsIterator.next();
					LOGGER.debug(logHeader + " Current relavent record timestamp [" + currRelevanceRecord.getTimestamp()
							+ "], identity [" + currRelevanceRecord.getIdentity() + "], latitude ["
							+ currRelevanceRecord.getLattitude() + "], longitude ["
							+ currRelevanceRecord.getLongtitude() + "],");

					/* Get distance between two identity coordinated */
					double currDistance = getDistance(sortedRecord.getLattitude(), sortedRecord.getLongtitude(),
							currRelevanceRecord.getLattitude(), currRelevanceRecord.getLongtitude());

					/*
					 * if current relevance identity is not in the meeting list and distance is less
					 * than defined proximity distance
					 */
					if (!meetingIdentities.contains(currRelevanceRecord.getIdentity())
							&& currDistance <= this.maxDistance) {

						LOGGER.debug("lat1: [" + sortedRecord.getLattitude() + "], long1 ["
								+ sortedRecord.getLongtitude() + "], lat2: [" + currRelevanceRecord.getLattitude()
								+ "], long2 [" + currRelevanceRecord.getLongtitude() + "], currDistance ["
								+ currDistance + "], max distance [" + this.maxDistance + "]");

						/* add current relevance identity into list */
						meetingIdentities.add(currRelevanceRecord.getIdentity());

						/*
						 * if current distance is more than previous distance, update to current
						 * distance
						 */
						if (currDistance > maxDistanceAmongMeeting) {
							maxDistanceAmongMeeting = currDistance;
						}

						/*
						 * if current relevance record timestamp is more than previous timestamp, update
						 * to current timestamp
						 */
						if (currRelevanceRecord.getTimestamp() > meetingEndTimestamp) {
							meetingEndTimestamp = currRelevanceRecord.getTimestamp();
						}
					}
				}

				/*
				 * if number of unique identities is more or equal to the min identities defined
				 */
				if (meetingIdentities.size() >= this.minimumObjectCount) {
					String identitiesGroup = String.join(",", meetingIdentities);
					int currProximityResultHash = identitiesGroup.hashCode();

					/* if exact identities string is not exist in hashmap */
					if (!proximityResultHash.containsKey(identitiesGroup.hashCode())) {
						LOGGER.info("======proximity found for ["+sortedRecord.getIdentity()+"], identities are ["+identitiesGroup+"], max distance is ["+(Math.round(maxDistanceAmongMeeting * 100.0) / 100.0)+"meters]" );
						proximityResult.addProximityResult(meetingCounter, sortedRecord.getLattitude(),
								sortedRecord.getLongtitude(), sortedRecord.getTimestamp(), meetingEndTimestamp,
								identitiesGroup, Math.round(maxDistanceAmongMeeting * 100.0) / 100.0);

						proximityResultHash.put(identitiesGroup.hashCode(), sortedRecord.getTimestamp());
						meetingCounter++;

						/*
						 * if identities string is exist in hashmap, check is the start timestamp of
						 * current finding is more than max gap compare to previous one
						 */
					} else if ((sortedRecord.getTimestamp() - proximityResultHash.get(identitiesGroup.hashCode())) > this.maxgap) {
						LOGGER.info("======proximity found for ["+sortedRecord.getIdentity()+"], identities are ["+identitiesGroup+"], max distance is ["+(Math.round(maxDistanceAmongMeeting * 100.0) / 100.0)+"meters]" );
						proximityResult.addProximityResult(meetingCounter, sortedRecord.getLattitude(),
								sortedRecord.getLongtitude(), sortedRecord.getTimestamp(), meetingEndTimestamp,
								identitiesGroup, Math.round(maxDistanceAmongMeeting * 100.0) / 100.0);

						proximityResultHash.put(identitiesGroup.hashCode(), sortedRecord.getTimestamp());
						meetingCounter++;
					}
				}
				currRelaventRecordsIterator = null;
				currRelaventRecords = null;
				meetingIdentities = null;
				sortedRecord = null;
			}

			FileWriter outputFile = new FileWriter(this.ouputPath + jobID + ".json");
			outputFile.write(gson.toJson(proximityResult));
			outputFile.close();

			LOGGER.debug("Sample JSON" + gson.toJson(proximityResult));

			sortedRecordsIterator = null;
			sortedRecords = null;
			proximityResultHash = null;
			proximityResult = null;

			if (connection != null && !connection.isClosed()) {
				try {
					dropTable(tableName);
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					LOGGER.error(logHeader + "  sql connection clode failed", e);
				}
			}

		} catch (ClassNotFoundException e1) {
			jobStatusCollection.getJobStatusCollection().put(jobID,
					new EsriStatusResponse(jobID, CommonReference.ESRI_STATUS_FAILED,
							"Job [" + jobID + "] org.hsqldb.jdbc.JDBCDriver class not found"));
			LOGGER.error(logHeader + " org.hsqldb.jdbc.JDBCDriver class not found", e1);
		} catch (SQLException e) {
			jobStatusCollection.getJobStatusCollection().put(jobID,
					new EsriStatusResponse(jobID, CommonReference.ESRI_STATUS_FAILED, "Job [" + jobID + "] sql error"));
			LOGGER.error(logHeader + " sql connection error", e);
		} catch (IOException e2) {
			jobStatusCollection.getJobStatusCollection().put(jobID, new EsriStatusResponse(jobID,
					CommonReference.ESRI_STATUS_FAILED, "Job [" + jobID + "] write to file failed"));
			LOGGER.error(logHeader + " write to file failed", e2);
		} finally {

			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				LOGGER.error(logHeader + "  sql connection clode failed", e);
			}
		}
	}

	private ArrayList<InputRecord> getRecordByTimestampPeriod(String tableName, String curreIdentitiy,
			long startTimestamp, long endTimestamp) throws SQLException {
		ArrayList<InputRecord> recordSet = new ArrayList();

		Statement stmt = this.connection.createStatement();
		ResultSet rs = stmt.executeQuery(
				"SELECT * from " + tableName + " WHERE identity <> '" + curreIdentitiy + "' AND timestamp >= "
						+ startTimestamp + " AND timestamp<=" + endTimestamp + " ORDER BY timestamp ASC");
		while (rs.next()) {
			recordSet.add(new InputRecord(rs.getLong("timestamp"), rs.getString("identity"), rs.getDouble("latitude"),
					rs.getDouble("longtitude")));
		}

		rs.close();
		stmt.close();

		return recordSet;
	}

	private List<InputRecord> getAllRecordsSortByTimestamp(String tableName) throws SQLException {
		List<InputRecord> recordSet = new ArrayList();

		Statement stmt = this.connection.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * from " + tableName + " ORDER BY timestamp ASC");
		while (rs.next()) {
			recordSet.add(new InputRecord(rs.getLong("timestamp"), rs.getString("identity"), rs.getDouble("latitude"),
					rs.getDouble("longtitude")));
		}

		rs.close();
		stmt.close();

		return recordSet;
	}

	private void createTable(String tableName) throws SQLException {
		Statement stmt = null;
		stmt = this.connection.createStatement();

		stmt.executeUpdate("CREATE TABLE " + tableName
				+ " (id int NOT NULL AUTO_INCREMENT, timestamp bigint NOT NULL,identity VARCHAR(50) NOT NULL, longtitude DOUBLE , latitude DOUBLE, PRIMARY KEY(id));");
		stmt.executeUpdate("CREATE INDEX tsid_" + tableName + " ON " + tableName + " (timestamp, identity)");
		stmt.close();
	}

	private void dropTable(String tableName) throws SQLException {
		Statement stmt = null;
		stmt = this.connection.createStatement();

		stmt.executeUpdate("DROP INDEX tsid_" + tableName);
		stmt.executeUpdate("DROP TABLE " + tableName);

		stmt.close();
	}

	private double getDistance(double lat1, double lon1, double lat2, double lon2) {
		GeodesicLine line = geod.InverseLine(lat1, lon1, lat2, lon2);
		return line.Distance();
	}

	public JobStatusCollection getJobStatusCollection() {
		return jobStatusCollection;
	}

	public void setJobStatusCollection(JobStatusCollection jobStatusCollection) {
		this.jobStatusCollection = jobStatusCollection;
	}

	public String getJobID() {
		return jobID;
	}

	public void setJobID(String jobID) {
		this.jobID = jobID;
	}

	public String getInputFilePath() {
		return inputFilePath;
	}

	public void setInputFilePath(String inputFilePath) {
		this.inputFilePath = inputFilePath;
	}

	public char getDelimiter() {
		return delimiter;
	}

	public void setDelimiter(char delimiter) {
		this.delimiter = delimiter;
	}

	public String getIdkey() {
		return idkey;
	}

	public void setIdkey(String idkey) {
		this.idkey = idkey;
	}

	public String getTimestampkey() {
		return timestampkey;
	}

	public void setTimestampkey(String timestampkey) {
		this.timestampkey = timestampkey;
	}

	public String getXkey() {
		return xkey;
	}

	public void setXkey(String xkey) {
		this.xkey = xkey;
	}

	public String getYkey() {
		return ykey;
	}

	public void setYkey(String ykey) {
		this.ykey = ykey;
	}

	public boolean isInclmillisec() {
		return inclmillisec;
	}

	public void setInclmillisec(boolean inclmillisec) {
		this.inclmillisec = inclmillisec;
	}

	public int getWindowSize() {
		return windowSize;
	}

	public void setWindowSize(int windowSize) {
		this.windowSize = windowSize;
	}

	public int getMaxDistance() {
		return maxDistance;
	}

	public void setMaxDistance(int maxDistance) {
		this.maxDistance = maxDistance;
	}

	public int getMinimumObjectCount() {
		return minimumObjectCount;
	}

	public void setMinimumObjectCount(int minimumObjectCount) {
		this.minimumObjectCount = minimumObjectCount;
	}

	public int getMaxgap() {
		return maxgap;
	}

	public void setMaxgap(int maxgap) {
		this.maxgap = maxgap;
	}

	public String[] getFilteredids() {
		return filteredids;
	}

	public void setFilteredids(String[] filteredids) {
		this.filteredids = filteredids;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getOuputPath() {
		return ouputPath;
	}

	public void setOuputPath(String ouputPath) {
		this.ouputPath = ouputPath;
	}

}

package com.ipfli.ipf.services.proximity.analaysis.config;

public class CommonReference {
	public static final String ESRI_STATUS_PROCESSING = "esriJobSubmitted";
	public static final String ESRI_STATUS_SUCCESS = "esriJobSucceeded";
	public static final String ESRI_STATUS_FAILED = "esriJobFailed";

}

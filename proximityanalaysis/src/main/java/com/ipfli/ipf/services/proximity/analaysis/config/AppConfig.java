package com.ipfli.ipf.services.proximity.analaysis.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.ipfli.ipf.services.proximity.analaysis.bean.JobStatusCollection;


@Configuration
public class AppConfig {
	
   @Bean("jobStatusCollection")
   public JobStatusCollection jobStatusCollection() {
      return new JobStatusCollection();
   }
   
   @Bean("taskExecutor")
   public TaskExecutor threadPoolTaskExecutor() {

       ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
       executor.setCorePoolSize(4);
       executor.setMaxPoolSize(4);
       executor.setThreadNamePrefix("pa_task_executer");
       executor.initialize();

       return executor;
   }   
}


package com.ipfli.ipf.services.proximity.analaysis.controller;

import java.io.File;
import java.nio.file.Paths;
import java.util.UUID;

import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.core.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ipfli.ipf.services.proximity.analaysis.bean.EsriStatusResponse;
import com.ipfli.ipf.services.proximity.analaysis.bean.JobStatusCollection;
import com.ipfli.ipf.services.proximity.analaysis.config.AppConfig;
import com.ipfli.ipf.services.proximity.analaysis.config.CommonReference;
import com.ipfli.ipf.services.proximity.analaysis.processor.ColocationJob;

@Slf4j
@RestController
@RequestMapping({ "/api/v1.0" })
public class JobController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ColocationJob.class);
	private AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
	
    private TaskExecutor taskExecutor = context.getBean("taskExecutor", TaskExecutor.class);

    JobStatusCollection jobStatusCollection = context.getBean("jobStatusCollection", JobStatusCollection.class);
    
/*	 
		http://localhost:8080?in_file=%7Burl%3A%22/mnt/IPF/ProximityAnalysis/Input/3abb5760-6349-11ea-8ff8-40b034e3cf9d.csv
		&delimiter=%3B
		&idkey=IDENTITY
		&timestampkey=TIME
		&xkey=LONGITUDE
		&ykey=LATITUDE
		&inclmillisec=true
		&windowSize=30
		&maxDistance=300
		&minimumObjectCount=1
		&maxgap=10
		&filteredids=%5B%5D
		&f=json
		&outgeojsonpath=/mnt/IPF/ProximityAnalysis/Output
*/				
				
	@PostMapping("/submitJob")
	EsriStatusResponse submitJob(
			@RequestParam("in_file")  String inputFilePath, 
			@RequestParam("delimiter")  String delimiter,
			@RequestParam("idkey")  String idkey,
			@RequestParam("timestampkey")  String timestampkey,
			@RequestParam("xkey")  String xkey,
			@RequestParam("ykey")  String ykey,
			@RequestParam("inclmillisec")  boolean inclmillisec,
			@RequestParam("windowSize")  int windowSize,
			@RequestParam("maxDistance")  int maxDistance,
			@RequestParam("minimumObjectCount")  int minimumObjectCount,
			@RequestParam("maxgap")  int maxgap,
			@RequestParam("filteredids")  String[] filteredids,
			@RequestParam("f")  String fileType,
			@RequestParam("outgeojsonpath")  String ouputPath			
			) {
			EsriStatusResponse currResp = null;

			File f = extractFilePath(inputFilePath);
			if(f.exists() && !f.isDirectory()) { 
				String jobID = f.getName().substring(0, f.getName().lastIndexOf('.'));	
				
				ColocationJob currColocationJob = new ColocationJob();
				currColocationJob.setJobStatusCollection(jobStatusCollection);
				currColocationJob.setJobID(jobID);
				currColocationJob.setInputFilePath( inputFilePath);
				currColocationJob.setDelimiter(delimiter.charAt(0));
				currColocationJob.setIdkey(idkey);
				currColocationJob.setTimestampkey(timestampkey);
				currColocationJob.setXkey(xkey);
				currColocationJob.setYkey(ykey);
				currColocationJob.setInclmillisec( inclmillisec);
				currColocationJob.setWindowSize( windowSize);
				currColocationJob.setMaxDistance(maxDistance);
				currColocationJob.setMinimumObjectCount( minimumObjectCount);
				currColocationJob.setMaxgap( maxgap);
				currColocationJob.setFilteredids( filteredids);
				currColocationJob.setFileType(fileType);
				currColocationJob.setOuputPath(ouputPath);
				
		        taskExecutor.execute(currColocationJob);
		        	
				currResp = new EsriStatusResponse(jobID, CommonReference.ESRI_STATUS_PROCESSING, "");
			} else {
				currResp = new EsriStatusResponse("ERROR", CommonReference.ESRI_STATUS_FAILED, "File ["+inputFilePath+"] not found or invalid");
				LOGGER.warn("Job submission failed. File ["+inputFilePath+"] not found or invalid");
			}
						
			return currResp;
	}

	private File extractFilePath(String inputFilePath) {
		log.info("Given path: {}", inputFilePath);
		return new File("");
	}

	@GetMapping("/jobs/{id}")
	EsriStatusResponse getJobStatus(@PathVariable String id) {
		EsriStatusResponse currResp = null;
		
		currResp = jobStatusCollection.getJobStatusCollection().get(id);	
		
		if (currResp == null) {
			currResp = new EsriStatusResponse(id, CommonReference.ESRI_STATUS_FAILED, "Job ["+id+"] not found!");
			LOGGER.warn("Job ["+id+"] not found!");			
		}
		
		return currResp;
	}
}
